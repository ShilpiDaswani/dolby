import { Component, OnInit } from '@angular/core';
declare var VoxeetSDK;
declare var navigator;
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  avengersNames = [
    "Thor",
    "Cap",
    "Tony Stark",
    "Black Panther",
    "Black Widow",
    "Hulk",
    "Spider-Man",
  ]
  lblDolbyVoice="";
  recordStatus="";
  videoList: any;
  randomName = this.avengersNames[Math.floor(Math.random() * this.avengersNames.length)]
  constructor() { }


  ngOnInit() {
    this.main();
  }
  async main() {
    // When a stream is added to the conference
    VoxeetSDK.conference.on('streamAdded', (participant, stream) => {
      if (stream.type === 'ScreenShare') {
          return this.addScreenShareNode(stream);
      }

      if (stream.getVideoTracks().length) {
          // Only add the video node if there is a video track
          this.addVideoNode(participant, stream);
      }

      this.addParticipantNode(participant);
  });

  // When a stream is updated
  VoxeetSDK.conference.on('streamUpdated', (participant, stream) => {
      if (stream.type === 'ScreenShare') return;

      if (stream.getVideoTracks().length) {
          // Only add the video node if there is a video track
          this.addVideoNode(participant, stream);
      } else {
          this.removeVideoNode(participant);
      }
  });

  // When a stream is removed from the conference
  VoxeetSDK.conference.on('streamRemoved', (participant, stream) => {
      if (stream.type === 'ScreenShare') {
          return this.removeScreenShareNode();
      }

      this.removeVideoNode(participant);
      this.removeParticipantNode(participant);
  });

    VoxeetSDK.initialize("RWiM79AQ9nT__brpd06UzA==", "hSYUrlTDBH_dROyoaw7AOuzQJS6t9whL64omyOHr4jk=");
    try {
      // Open the session here !!!!
      await VoxeetSDK.session.open({
        name: this.randomName, params: {
          dolbyVoice: true,
        }
      });
    } catch (e) {
      alert('Something went wrong : ' + e)
    }
  }

  createMeeting(conferenceAlias) {
    let conferenceParams = {
      liveRecording: false,
      rtcpMode: "average", // worst, average, max
      ttl: 0,
      videoCodec: "H264", // H264, VP8
      dolbyVoice: true
    };
    let conferenceOptions = {
      alias: conferenceAlias,
      params: conferenceParams
    };
    VoxeetSDK.conference.create(conferenceOptions)
      .then((conference) => {
        this.joinMeeting(conference);
      })
      .catch((e) => console.log('Something wrong happened : ' + e))
  };
  joinMeeting(conference) {

    const joinOptions = {
      constraints: {
        audio: true,
        video: true
      },
      simulcast: false
    };

    VoxeetSDK.conference.join(conference, joinOptions)
      .then((conf) => {
        console.log("conference joined");
        this.lblDolbyVoice = `Dolby Voice is ${conf.params.dolbyVoice ? 'On' : 'Off'}.`;
        //this.startVideo();
        //joinButton.disabled = true;
      })
      .catch((e) => console.log('Something wrong happened : ' + e))
    // VoxeetSDK.conference.join(conference,{})
    // .then(() => {
    //     //joinButton.disabled = true
    //     // Enable the leaveButton after resolving the join promise
    //     //leaveButton.disabled = false
    // })
  }
 
  
  leaveMeeting() {
    VoxeetSDK.conference
      .leave()
      .then(() => {
        alert("meeting left");
        this.lblDolbyVoice='';
        // joinButton.disabled = false
        // leaveButton.disabled = true
      })
      .catch(err => {
        console.log(err)
      })
  }
  startVideo() {
    VoxeetSDK.conference.startVideo(VoxeetSDK.session.participant).then(() => {
      // startVideoBtn.disabled = true
      console.log("video strated");
    })
  }
  stopVideo() {
    VoxeetSDK.conference.stopVideo(VoxeetSDK.session.participant).then(() => {
      //stopVideoBtn.disabled = true
      //startVideoBtn.disabled = false
      alert("video is stopped");
    })
  }

  startAudio() {
    // Start sharing the Audio with the other participants
    VoxeetSDK.conference.startAudio(VoxeetSDK.session.participant)
      .then(() => {
        console.log("audio started");
        //startAudioBtn.disabled = true;
        //stopAudioBtn.disabled = false;
      })
      .catch((e) => console.log(e));
  };
  stopAudio() {
    // Stop sharing the Audio with the other participants
    VoxeetSDK.conference.stopAudio(VoxeetSDK.session.participant)
      .then(() => {
        alert("audio stopped");
        //stopAudioBtn.disabled = true;
        //startAudioBtn.disabled = false;
      })
      .catch((e) => console.log(e));
  };
  startScreenShare() {
    VoxeetSDK.conference.startScreenShare()
      .then(() => {
        // startScreenShareBtn.disabled = true
      })
      .catch((e) => console.log(e))
  }

  stopScreenShare() {
    VoxeetSDK.conference.stopScreenShare()
      .then(() => {
        //startScreenShareBtn.disabled = false
        //stopScreenShareBtn.disabled = true
      })
      .catch((e) => console.log(e))
  }
  startRecording() {
    
    VoxeetSDK.recording.start()
      .then(() => {
        this.recordStatus = "Recording"
      })
      .catch(err => {
        alert("Cannnot record the conference")
      })
  }
  stopRecording() {
    VoxeetSDK.recording.stop()
      .then(() => {
        alert("stop sharing");
        // startRecordingBtn.disabled = false
        //stopRecordingBtn.disabled = true
        this.recordStatus = ''
      })
      .catch((e) => console.log(e))
  }

  // Add a video stream to the web page
   addVideoNode(participant, stream)  {
    let videoNode = document.getElementById('video-' + participant.id);
    if (!videoNode) {
      videoNode = document.createElement('video');

      videoNode.setAttribute('id', 'video-' + participant.id);
      videoNode.setAttribute('height', '240');
      videoNode.setAttribute('width', '320');
      videoNode.setAttribute("playsinline", JSON.parse('true'));
     // videoNode.m = true;
      videoNode.setAttribute("autoplay", 'autoplay');
      //videoNode.style = 'background: gray;';

      const videoContainer = document.getElementById('video-container');
      videoContainer.appendChild(videoNode);
    }

    navigator.attachMediaStream(videoNode, stream);
  };

  // Remove the video streem from the web page
   removeVideoNode(participant)  {
    let videoNode = document.getElementById('video-' + participant.id);

    if (videoNode) {
      videoNode.parentNode.removeChild(videoNode);
    }
  };

  // Add a new participant to the list
  addParticipantNode (participant) {
    // If the participant is the current session user, don't add himself to the list
    if (participant.id === VoxeetSDK.session.participant.id) return;

    let participantNode = document.createElement('li');
    participantNode.setAttribute('id', 'participant-' + participant.id);
    participantNode.innerText = `${participant.info.name}`;

    const participantsList = document.getElementById('participants-list');
    participantsList.appendChild(participantNode);
  };

  // Remove a participant from the list
   removeParticipantNode (participant) {
    let participantNode = document.getElementById('participant-' + participant.id);

    if (participantNode) {
      participantNode.parentNode.removeChild(participantNode);
    }
  };
  clickMe(){
    alert("I am called");
  }

  // Add a screen share stream to the web page
   addScreenShareNode (stream) {
    let screenShareNode = document.getElementById('screenshare');

    if (screenShareNode) {
      return alert('There is already a participant sharing a screen!');
    }

    screenShareNode = document.createElement('video');
    screenShareNode.setAttribute('id', 'screenshare');
    //screenShareNode.autoplay = 'autoplay';
   navigator.attachMediaStream(screenShareNode, stream);

    const screenShareContainer = document.getElementById('screenshare-container');
    screenShareContainer.appendChild(screenShareNode);
  }

  // Remove the screen share stream from the web page
   removeScreenShareNode () {
    let screenShareNode = document.getElementById('screenshare');

    if (screenShareNode) {
      screenShareNode.parentNode.removeChild(screenShareNode);
    }

    const startScreenShareBtn = document.getElementById('start-screenshare-btn');
   // startScreenShareBtn.disabled = false;

    const stopScreenShareBtn = document.getElementById('stop-screenshare-btn');
   // stopScreenShareBtn.disabled = true;
  }
}
